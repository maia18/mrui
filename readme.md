# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official NodeJS documentation](https://nodejs.org/es/docs/)
* [Official Express documentation](https://expressjs.com/en/api.html)

### Additional Links
* [NodeJS Package Management](https://www.npmjs.com/)

### Run the project

Start the nodeJS server by typing the following command from the nodejs folder:

nodemon app.js 