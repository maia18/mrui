export interface PatientInfo
{
  id: string,
  docType: string,
  docId: string,
  lastname: string,
  name: string,
  gender: string,
  birthdate: string,
  birthplace: string,
  address: string,
  phone: string,
  email: string,
  role: string,
  status: string,
  child: string
}

export interface MedicalRecord
{
  reportDate: string,
  studyName: string,
  diagnosisDescription: string,
  diagnosisComments: string
  user: SystemUser,
  id: string
}

export interface SystemUser
{
  userId: string,
  documentType: string,
  documentNumber: string,
  lastName: string,
  name: string,
  gender: string,
  birthDate: string,
  birthPlace: string,
  address: string,
  phone: string,
  department: string,
  email: string,
  role: string
}
