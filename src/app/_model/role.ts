export enum Role
{
    admin = "ROLE_ADMIN",
    viewer = "ROLE_VIEWER",
    prof = "ROLE_PROF",
    rman = "ROLE_RMAN",
    feeder = "ROLE_FEEDER"

}
