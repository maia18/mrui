import { Component } from '@angular/core';
import { User } from './_model';
import {Router} from "@angular/router";
import {AuthenticationService} from "./_services";
import { Role } from './_model/role';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  currentUser: User;
  roleToShow: string = '';

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) 
  {
    this.authenticationService.currentUser.subscribe(
      x => this.currentUser = x
    ); 
    
  }
  parseRoleName(rolename: String)
  {
    switch(rolename)
    {
      case "ROLE_ADMIN":
        return "Administrator";
      case "ROLE_RMAN":
        return "Role Manager";
      case "ROLE_VIEWER":
        return "Viewer";
      case "ROLE_PROF":
        return "Professional";
      default:
        return "Feeder";
    }
  }
  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

  goToDashBoard(){
    this.router.navigate(['/dashboard']);
  }
  goToUsers(){
    this.router.navigate(['/users/search']);
  }
  goTopatients(){
    this.router.navigate(['/patients/search']);
  }
  goToPACS(){
    this.router.navigate(['/pacs']);
  }
  goToProfile(){
    this.router.navigate(['/profile']);
  }
  toToChangePassword(){
    this.router.navigate(['/changepassword']);
  }
}
