import { Component, OnInit } from '@angular/core';
import {Validators} from "@angular/forms";
import { User } from '../_model';
import {Router} from "@angular/router";
import {AuthenticationService} from "../_services";
import { Role } from '../_model/role';

@Component({ templateUrl: 'testres.component.html',
            styleUrls: ['./testres.component.css']
})
export class TestresComponent
{
  currentUser: User;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x); 
  }


}
