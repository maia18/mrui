import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login';
import {TestresComponent} from "./testres";
import {AuthorizationGuard} from "./_services/AuthorizationGuard";
import {Role} from "./_model/role";
import { ProfileComponent } from './users/profile/profile.component';
import { ChangepasswordComponent } from './users/changepassword/changepassword.component';
import {PatientcreationComponent} from './Patients/patientcreation/patientcreation.component';
import {UsersearchComponent} from './users/search/usersearch.component';
import {UsercreationComponent} from './users/create/usercreation.component';
import {UsersviewComponent} from './users/usersview/usersview.component';
import {UserseditComponent} from './users/usersedit/usersedit.component';
import {PatientseditComponent} from './Patients/patientsedit/patientsedit.component';
import {PatientssearchComponent} from "./Patients/search/patientssearch.component";
import {PatientsviewComponent} from "./Patients/view/patientsview.component"

const routes: Routes = [
  { path: 'dashboard', component: TestresComponent, canActivate: [AuthorizationGuard]},
  { path: 'abc', component: TestresComponent, canActivate: [AuthorizationGuard], data: { roles: [Role.admin] }},
  { path: 'login', component: LoginComponent },
  { path: 'users/search', component: UsersearchComponent, canActivate: [AuthorizationGuard], data: {roles: [Role.admin, Role.rman, Role.viewer]}},
  { path: 'register', component: UsercreationComponent, canActivate: [AuthorizationGuard], data: {roles: [Role.admin, Role.rman]}},
  { path: 'users/view/:userId', component: UsersviewComponent, canActivate: [AuthorizationGuard], data: {roles: [Role.admin, Role.rman]}},
  { path: 'users/edit/:userId', component: UserseditComponent, canActivate: [AuthorizationGuard], data: {roles: [Role.admin, Role.rman]}},
  { path: 'users', redirectTo: 'users/search'},
  { path: 'patients/create', component: PatientcreationComponent, canActivate: [AuthorizationGuard], data: {roles: [Role.admin, Role.rman]}},
  { path: 'patients/edit/:patientId', component: PatientseditComponent, canActivate: [AuthorizationGuard], data: {roles: [Role.admin, Role.rman]}},
  { path: 'profile', component: ProfileComponent, canActivate: [AuthorizationGuard]},
  { path: 'changepassword', component: ChangepasswordComponent, canActivate: [AuthorizationGuard]},
  { path: 'patients/search', component: PatientssearchComponent, canActivate: [AuthorizationGuard], data: {roles: [Role.admin, Role.rman, Role.prof, Role.viewer]}},
  { path: 'patients', redirectTo: 'patients/search'},
  { path: 'patients/view/:patientId', component: PatientsviewComponent, canActivate: [AuthorizationGuard], data: {roles: [Role.admin, Role.rman, Role.prof, Role.viewer]}},
  { path: '**', redirectTo: 'dashboard' }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
