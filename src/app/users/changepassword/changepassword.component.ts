import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/_services/UserService';
import { AuthenticationService } from 'src/app/_services';
import {FormBuilder, FormGroup, FormControl, FormGroupDirective, NgForm, Validators, FormsModule} from '@angular/forms';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {

  passwordChangeForm: FormGroup;
  error:boolean = false;
  updated: boolean = false;

  constructor(private fb: FormBuilder, private userAuth: AuthenticationService, private route: ActivatedRoute, private theUserService: UserService, private router: Router) {
    this.passwordChangeForm = fb.group({  
      'password': [null, Validators.compose([Validators.required, Validators.minLength(8)])],
      'verifypassword': [null, Validators.required]
      },
      {
        validators: MustMatch('password', 'verifypassword')
      });
  }

  ngOnInit()
  {

  }

  onFormSubmit(form: NgForm)  
  {
      if (this.passwordChangeForm.invalid) 
      {
        return;
      }
      else{
      //First, try to create with current token
      let errorHolder = {errorMsg : ''}
      let user = JSON.parse(localStorage.getItem('currentUser'));
      this.theUserService.changePassword(JSON.stringify(form), user.access_token).subscribe(
        resp => {
          if(resp.status == 200){ //User created 
              console.log("Password updated sucessfuly");
              this.updated = true;
              return;
          }else{
            this.error = true;
            return
          }
          console.log(resp)},
        err => {
          //If error, refresh token and attempt to create again
          if(err.status == 409){ //User already exists
            console.log("this");
          }else{
            console.log(err);
            console.log("Trying to refresh token");
            //this.userAuth.refreshToken(errorHolder, () => this.changePassword(form));
            return;
          }
          } 
      );
    }
  }

    changePassword(form: NgForm)
    {
      let user = JSON.parse(localStorage.getItem('currentUser'));
      this.theUserService.changePassword(JSON.stringify(form), user.access_token).subscribe(
        resp => {
          if(resp.status == 200){ //User created 
              console.log("Password updated sucessfuly");
              this.updated = true;
              return;
          }else{
            this.error = true;
            return
          }
          console.log(resp)},
        err => {
          //If error, refresh token and attempt to create again
          console.log(err);
          return;
          } 
      );
    }


}

export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  }
}