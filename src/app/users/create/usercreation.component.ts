import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { UserService } from '../../_services/UserService';
import {AuthenticationService} from '../../_services/AuthenticationService';
import { Router } from '@angular/router';


@Component({
  selector: 'app-usercreation',
  templateUrl: './usercreation.component.html',
  styleUrls: ['./usercreation.component.css']
})
export class UsercreationComponent implements OnInit {

  userCreationForm: FormGroup;  
  created: boolean=false;
  exists: boolean=false;
  error: boolean=false;
  minDate = new Date(1920, 0, 1);
  maxDate = new Date();


  constructor(private fb: FormBuilder, private userPost: UserService, private userAuth: AuthenticationService, private router: Router) {

	this.userCreationForm = fb.group({  
    'userId': [null, Validators.required],
	  'documentType': [null, Validators.required],
    'documentNumber': [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])], 
    'name' : [null, Validators.required],  
	  'lastName': [null, Validators.required], 
	  'birthDate': [null, Validators.required],
	  'birthPlace': [null, Validators.required],
    'gender': [null, Validators.required],
    'phone' : [null, Validators.compose([Validators.required, Validators.minLength(10)])],
	  'address': [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])],
	  'department': [null, Validators.required],
    'role': [null, Validators.required],
    'email': [null, Validators.compose([Validators.required, Validators.email])],
    'password': [null, Validators.compose([Validators.required, Validators.minLength(8)])],
    'verifypassword': [null, Validators.required]
    },
    {
      validators: MustMatch('password', 'verifypassword')
    });
  }

  ngOnInit() {
  }

  get f() {
    return this.userCreationForm.controls;
  }

  onFormSubmit(form: any)
  {
    if (this.userCreationForm.invalid) 
    {
      return;
    }

    let date = form.birthDate;
    form.birthDate = this.formatDate(date);

    //First, try to create with current token
    let user = JSON.parse(localStorage.getItem('currentUser'));
    this.userPost.createUser(JSON.stringify(form), user.access_token).subscribe(
      resp => {
        if(resp.status == 201){ //User created
            console.log("User created sucessfuly");
            this.created = true;
            return;
        }
        else
        {
          this.error = true;
          return
        }
        console.log(resp)
      },
      err => {
        //If error, refresh token and attempt to create again
        if (err.status == 409) { //User already exists
          console.log("User already exists");
          this.exists = true;
        } else {
          console.log(err);
          console.log("Unknown error");
          this.error = true;
          return;
        }
      }
    );
  }

  formatDate(date: Date): String
  {
    let d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('/');
  }
}

export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  }
}


