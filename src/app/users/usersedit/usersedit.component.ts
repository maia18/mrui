import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/_services/UserService';
import { AuthenticationService } from 'src/app/_services';
import {FormBuilder, FormGroup, FormControl, FormGroupDirective, NgForm, Validators, FormsModule} from '@angular/forms';

@Component({
  selector: 'app-usersedit',
  templateUrl: './usersedit.component.html',
  styleUrls: ['./usersedit.component.css']
})
export class UserseditComponent implements OnInit {

  userEditionForm: FormGroup; 
  updated: boolean=false;
  error: boolean=false;
  notFound: boolean=false;
  minDate = new Date(1920, 0, 1);
  maxDate = new Date();

  userId: string = '';
  docType: string = '';
  doctTypeWithoutFormat: string = '';
  docNumber: string = '';
  lastname: string = '';
  name: string= '';
  gender: string = '';
  birthdate : Date= new Date();
  birthplace: string='';
  address: string='';
  phone: string='';
  role: string='';
  department: string='';
  email: string='';
  password: string='';
  selectedDocType:string = '';
  selectedGender:string = '';

  constructor(private fb: FormBuilder, private userAuth: AuthenticationService, private route: ActivatedRoute, private theUserService: UserService, private router: Router) {
    this.userEditionForm = fb.group({  
      'documentType': [null, Validators.required],
      'documentNumber': [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])], 
      'name' : [null, Validators.required],  
      'lastName': [null, Validators.required], 
      'birthDate': [null, Validators.required],
      'birthPlace': [null, Validators.required],
      'gender': [null, Validators.required],
      'phone' : [null, Validators.compose([Validators.required, Validators.minLength(10)])],
      'address': [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])],
      'department': [null, Validators.required],
      'role': [null, Validators.required],
      'email': [null, Validators.compose([Validators.required, Validators.email])],
      'password' : [null]
      });
   }

  ngOnInit() {
    let errorHolder = {errorMsg : ''};
    //this.userId = this.route.snapshot.paramMap.get("userId");
    this.route.paramMap.subscribe(params => {
    this.userId = params.get("userId")
    })
    let user = JSON.parse(localStorage.getItem('currentUser'));
    this.theUserService.getUserInfo(this.userId, user.access_token).subscribe(
      resp => {
        if(resp.status == 404)
        {
          console.log("User not found");
        }
        console.log(resp.body);
        //Set values of variables based on GET response
        this.setVariablesFromGET(resp.body);
        //Set values of form (necessary so that on submit the values are actually taken)
        this.setFormValues(); 
      },
      err => {
        if(err.status==404)
        {
          this.notFound=true;
          return;
        }
        //this.userAuth.refreshToken(errorHolder, () => this.getUserInfo());
      }
    )
  }

  getUserInfo()
  {
    let user = JSON.parse(localStorage.getItem('currentUser'));
    this.theUserService.getUserInfo(this.userId, user.access_token).subscribe(
      resp => {
        if(resp.status == 404)
        {
          console.log("User not found");
          this.notFound = true;
        }
        console.log(resp.body);
        
        //Set values for variables according to http GET response
        this.setVariablesFromGET(resp.body);
        //Set values of form
        this.setFormValues();
      },
      err => {
        if(err.status==404)
        {
          this.notFound=true;
          return;
        }
        console.log(err);
      }
    )
  }

  setVariablesFromGET(response: Object)
  {
    this.docType = this.formatDocType(response['documentType']);
    this.doctTypeWithoutFormat = response['documentType'];
    this.docNumber = response['documentNumber'];
    this.lastname = response['lastName'];
    this.name = response['name'];
    this.gender = response['gender'];
    this.birthdate = new Date(Date.parse(response['birthDate']));
    this.birthplace = response['birthPlace'];
    this.address = response['address'];
    this.phone = response['phone'];
    this.department = response['department'];
    this.email = response['email'];
    this.selectedDocType = response['documentType'];
    this.role = response['role'];
  }

  setFormValues()
  {
    this.userEditionForm.controls['documentType'].setValue(this.doctTypeWithoutFormat);
    this.userEditionForm.controls['documentNumber'].setValue(this.docNumber);
    this.userEditionForm.controls['name'].setValue(this.name);
    this.userEditionForm.controls['lastName'].setValue(this.lastname);
    this.userEditionForm.controls['birthDate'].setValue(this.birthdate);
    this.userEditionForm.controls['birthPlace'].setValue(this.birthplace);
    this.userEditionForm.controls['gender'].setValue(this.gender);
    this.userEditionForm.controls['phone'].setValue(this.phone);
    this.userEditionForm.controls['address'].setValue(this.address);
    this.userEditionForm.controls['department'].setValue(this.department);
    this.userEditionForm.controls['role'].setValue(this.role);
    this.userEditionForm.controls['email'].setValue(this.email);
  }

  formatDocType(doctype: string)
  {
    if(doctype=="PASSPORT")
    {
      return "Passport";
    }else if(doctype=="NATIONAL_ID")
    {
      return "National ID";
    }else if(doctype=="RESIDENCE_PERMIT_NUMBER")
    {
      return "Residence Permit";
    }else
    {
      return "Other";
    }
  }

  formatDate(date: Date): String
  {
    let d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('/');
  }

  onFormSubmit(form: any)
  {
      console.log(form);


    if (this.userEditionForm.invalid) 
    {
      return;
    }

    let date = form.birthDate;
    form.birthDate = this.formatDate(date);

    //First, try to create with current token
    let errorHolder = {errorMsg : ''}
    let user = JSON.parse(localStorage.getItem('currentUser'));
    this.theUserService.putUserInfo(JSON.stringify(form), this.userId, user.access_token).subscribe(
      resp => {
        if(resp.status == 200){ //User created
            console.log("User updated sucessfuly");
            this.updated = true;
            return;
        }else{
          this.error = true;
          return
        }
        console.log(resp)},
      err => {
            console.log(err);
            return;
        }
    );
  }

  updateUser(form: NgForm)
  {
    let user = JSON.parse(localStorage.getItem('currentUser'));
      this.theUserService.putUserInfo(JSON.stringify(form), this.userId, user.access_token).subscribe(
        resp => {
          if(resp.status == 200){ //User created 
              console.log("User updated sucessfuly");
              this.updated = true;
              return;
          }else{
            this.error = true;
            return
          }
          console.log(resp)},
        err => {
            console.log(err);
            return;
          } 
      );
  }

  goToUser()
  {
    this.router.navigate(['/users/view/'+this.userId]);
  }

}
