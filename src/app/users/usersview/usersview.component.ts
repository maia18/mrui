import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/_services/UserService';
import { AuthenticationService } from 'src/app/_services';

@Component({
  selector: 'app-usersview',
  templateUrl: './usersview.component.html',
  styleUrls: ['./usersview.component.css']
})
export class UsersviewComponent implements OnInit {

  notFound: boolean=false;
  userId: string = '';
  docType: string = '';
  docNumber: string = '';
  lastname: string = '';
  name: string= '';
  gender: string = '';
  birthdate : string='';
  birthplace: string='';
  address: string='';
  phone: string='';
  department: string='';
  email: string='';

  constructor(private userAuth: AuthenticationService, private route: ActivatedRoute, private theUserService: UserService) { 
    
  }

  ngOnInit() {

    let errorHolder = {errorMsg : ''};
    //this.userId = this.route.snapshot.paramMap.get("userId");
    this.route.paramMap.subscribe(params => {
      this.userId = params.get("userId")
    })
    let user = JSON.parse(localStorage.getItem('currentUser'));
    this.theUserService.getUserInfo(this.userId, user.access_token).subscribe(
      resp => {
		  console.log(resp.status);
        console.log(resp.body);
        this.docType = this.formatDocType(resp.body['documentType']);
        this.docNumber = resp.body['documentNumber'];
        this.lastname = resp.body['lastName'];
        this.name = resp.body['name'];
        this.gender = resp.body['gender'];
        this.birthdate = resp.body['birthDate'].slice(0,10);
        this.birthplace = resp.body['birthPlace'];
        this.address = resp.body['address'];
        this.phone = resp.body['phone'];
        this.department = resp.body['department'];
        this.email = resp.body['email'];
      },
      err => {
		  if(err.status==404)
		  {
			  this.notFound=true;
			  return;
		  }
        console.log(err.status);
        //this.userAuth.refreshToken(errorHolder, () => this.getUserInfo());
      }
    )
  
  }

  getUserInfo()
  {
    let user = JSON.parse(localStorage.getItem('currentUser'));
    this.theUserService.getUserInfo(this.userId, user.access_token).subscribe(
      resp => {
		  console.log(resp.status);
        console.log(resp.body);
      },
      err => {
		  if(err.status==404)
		  {
			  this.notFound=true;
			  return;
		  }
        console.log(err);
      }
    )
  }

  formatDocType(doctype: string)
  {
    if(doctype=="PASSPORT")
    {
      return "Passport";
    }else if(doctype=="NATIONAL_ID")
    {
      return "National ID";
    }else if(doctype=="RESIDENCE_PERMIT_NUMBER")
    {
      return "Residence Permit";
    }else
    {
      return "Other";
    }
  }

}
