import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, FormControl, FormGroupDirective, NgForm, Validators, FormsModule} from '@angular/forms';
import {UserInfo, UserService} from '../../_services/UserService';
import {AuthenticationService} from '../../_services/AuthenticationService';
import { Router} from '@angular/router';


@Component({
  selector: 'app-searchuser',
  templateUrl: './usersearch.component.html',
  styleUrls: ['./usersearch.component.css']
})

export class UsersearchComponent implements OnInit
{
    userSearchForm: FormGroup;
    created: boolean=false;
    exists: boolean=false;
    error: boolean=false;
    displayedColumns = ['username', 'fullname', 'documentype', 'document', 'action'];
    dataSource = ELEMENT_DATA;


    constructor(
      private fb: FormBuilder,
      private userService: UserService,
      private userAuth: AuthenticationService,
      private router: Router)
    {

      this.userSearchForm =
        fb.group({
          'userId': [""],
          'documentNumber': [""],
          'name' : [""],
          'lastName': [""]
          });

    }
    ngOnInit()
    {
        let users = this.userService.retrieveAllUsersBy("","","","");
        users.subscribe(
            respo =>
                {
                    let entries = this.mapEntries(respo);
                    this.dataSource = entries;
                }
        )
    }

    private mapEntries(users: any) : UserEntry[]
    {
        let entries: UserEntry[] = new Array(users.length);
        for (var i = 0; i < users.length; i++)
        {
            let entry : UserEntry = new class implements UserEntry {
                document: string;
                documentype: string;
                fullname: string;
                username: string;
                edit: string;
                medrec: string;
                view: string;
            };
            entry.document = users[i].documentNumber;
            entry.documentype = users[i].documentType;
            entry.fullname = users[i].name + " " + users[i].lastName;
            entry.username = users[i].userId;
            entry.view = "/users/view/" + users[i].userId;
            entry.edit = "/users/edit/" + users[i].userId;
            entries[i] = entry;
        }
        return entries
    }

    onFormSubmit(form: UserInfo)
    {
        let users =
            this.userService.retrieveAllUsersBy(
                form.userId, form.documentNumber, form.name,form.lastName);
        users.subscribe(
            respo =>
            {
                let entries = this.mapEntries(respo);
                this.dataSource = entries;
            }
        )
    }
}

export interface UserEntry
{
    username: string;
    fullname: string;
    documentype: string;
    document: string;
    edit: string;
    view: string;
}

const ELEMENT_DATA: UserEntry[] = [
  {username: 'lpausini', fullname: 'Laura Pausini', documentype: 'Passport', document: 'AOPLL', edit: '/hello', view: '/hello'}
];
