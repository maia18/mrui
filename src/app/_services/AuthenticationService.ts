import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';

import { User } from '../_model';
import {map} from "rxjs/operators";

const initialAuthenticationHeaders = {
    headers: new HttpHeaders({
        'Content-Type':  'application/x-www-form-urlencoded',
        'Authorization': 'Basic '+ btoa('webClient:webpassword')
    })
};

@Injectable({ providedIn: 'root' })
export class AuthenticationService
{
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;


    constructor(private http: HttpClient)
    {
        this.currentUserSubject =
            new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public buildAuthenticationHeaders(contentType : string, token : string): {headers: HttpHeaders}
    {
        return {
          headers: new HttpHeaders({
            'Content-Type':  contentType,
            'Authorization': ('Bearer '+ token)
        })};
    }
    public get currentUserValue(): any
    {
        return this.currentUserSubject.value;
    }

    public getInitialAuthenticationHeaders(): {headers: HttpHeaders}
    {
        return initialAuthenticationHeaders;
    }

    login(username: string, password: string)
    {
        let bodyObj = `username=${username}&password=${password}&grant_type=password`;
        return this.http.post<User>(`http://localhost:8080/oauth/token`, bodyObj, initialAuthenticationHeaders)
        .pipe(
            map(
                user => {

                    if (user && user.access_token)
                    {
                        this.currentUserSubject.next(user);
                    }

                    return user;
                }));
    }

    public buildAuthHeaders()
    {
        return this.buildAuthenticationHeaders("application/json", this.currentUserValue.access_token);
    }

    fetchRole()
    {
        if (this.currentUserValue) {
            let customHeaders =
                this.buildAuthenticationHeaders("application/json", this.currentUserValue.access_token);
            return this.http.get<any>(`http://localhost:8080/api/me`, customHeaders)
                .pipe(
                    map(
                        user => {

                            if (user && user.role)
                            {
                                this.currentUserSubject.value.role = user.role;
                                this.currentUserSubject.value.username = user.username;
                                localStorage.setItem('currentUser', JSON.stringify(this.currentUserSubject.value));
                            }

                        }
                    )
                )
        }
        return null;
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}
