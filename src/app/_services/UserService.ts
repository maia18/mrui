import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../_services';

import {map} from "rxjs/operators";
import {Observable} from "rxjs";

const USERS_ENDPOINT = "http://localhost:8080/api/users";

@Injectable({ providedIn: 'root' })
export class UserService
{
    constructor(private http: HttpClient, private auth: AuthenticationService) {
    }

    createUser(json: string, authToken: string){
        const bearerTokenHeader = {
            headers: new HttpHeaders({
                'Content-Type':  'application/json',
                'Authorization': 'Bearer ' + authToken
            })
        };
        const options = {
            headers: bearerTokenHeader.headers,
            observe: "response" as 'body', // to display the full response & as 'body' for type cast
        };
        console.log(bearerTokenHeader);
        return this.http.post(USERS_ENDPOINT, json, {headers: bearerTokenHeader.headers, observe: "response"});
    }

    retrieveAllUsersBy(id: string, document: string, name: string, lastname: string) : Observable<UserInfo[]>
    {
        const requestUrl = `${USERS_ENDPOINT}?userid=${id}&document=${document}&name=${name}&lastname=${lastname}`;

        return this.http.get<UserInfo[]>(
            requestUrl, {headers: this.auth.buildAuthHeaders().headers, observe: "response"})
            .pipe(
                map(res => {
                    return res.body as UserInfo[];
                }));
    }

    getUserInfo(userID: string, authToken:string){
        const bearerTokenHeader = {
            headers: new HttpHeaders({
                //'Content-Type':  'application/json',
                'Authorization': 'Bearer ' + authToken
            })
        };
        const options = {
            headers: bearerTokenHeader.headers,
            observe: "response" as 'body', // to display the full response & as 'body' for type cast
        };
        let url = 'http://localhost:8080/api/users/' + userID;
        return this.http.get(url, {headers: bearerTokenHeader.headers, observe: "response"})
    }

    getMyInfo(authToken:string){
        const bearerTokenHeader = {
            headers: new HttpHeaders({
                //'Content-Type':  'application/json',
                'Authorization': 'Bearer ' + authToken
            })
        };
        const options = {
            headers: bearerTokenHeader.headers,
            observe: "response" as 'body', // to display the full response & as 'body' for type cast
        };
        let url = 'http://localhost:8080/api/me/allinfo'
        return this.http.get(url, {headers: bearerTokenHeader.headers, observe: "response"})
    }

    putUserInfo(json: string, userID: string, authToken: string){
        const bearerTokenHeader = {
            headers: new HttpHeaders({
                'Content-Type':  'application/json',
                'Authorization': 'Bearer ' + authToken
            })
        };   
        const options = {
            headers: bearerTokenHeader.headers,
            observe: "response" as 'body', // to display the full response & as 'body' for type cast
        };   
        let url =  'http://localhost:8080/api/users/' + userID;
        return this.http.put(url, json, {headers: bearerTokenHeader.headers, observe: "response"});
    }

    changePassword(json:string, authToken: string)
    {
        const bearerTokenHeader = {
            headers: new HttpHeaders({
                'Content-Type':  'application/json',
                'Authorization': 'Bearer ' + authToken
            })
        };   
        const options = {
            headers: bearerTokenHeader.headers,
            observe: "response" as 'body', // to display the full response & as 'body' for type cast
        };         
        let url =  'http://localhost:8080/api/me/changepass';
        return this.http.put(url, json, {headers: bearerTokenHeader.headers, observe: "response"}); 
    }
}

export interface UserInfo
{
    userId: string,
    documentType: string,
    documentNumber: string,
    lastName: string,
    name: string,
    gender: string,
    birthDate: string,
    birthPlace: string,
    address: string,
    phone: string,
    department: string,
    email: string,
    role: string
}