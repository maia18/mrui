import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpParams,
  HttpRequest,
  HttpResponse
} from "@angular/common/http";
import {Router} from "@angular/router";
import {User} from "../_model";
import {catchError, map} from "rxjs/operators";
import {AuthenticationService} from "./AuthenticationService";
import {Injectable} from "@angular/core";
import {Observable, throwError} from "rxjs";

@Injectable()
export class AuthInterceptor implements HttpInterceptor
{
    constructor(private router: Router, private  auth: AuthenticationService){}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>
    {
        return next.handle(request).pipe(
            catchError(
                (error: HttpErrorResponse) => {
                    console.log(error);
                    return throwError(error);
                    if (!this.auth.currentUser ||!error || !(error.status === 401))
                    {
                        return throwError(error);
                    }

                    /*const payload = new HttpParams()
                      .set('refresh_token', this.auth.currentUserValue().refresh_token)
                      .set('grant_type', 'refresh_token');
                    return this.http.post<User>(
                        `http://localhost:8080/oauth/token`, payload, this.auth.getInitialAuthenticationHeaders())
                        .pipe(
                            map(
                                user => {

                                    if (user && user.access_token)
                                    {
                                        this.auth.currentUser.refresh_token = user.refresh_token;
                                        this.auth.currentUser.access_token = user.access_token;
                                        localStorage.setItem('currentUser', JSON.stringify(this.auth.currentUserValue));
                                    }
                                    return user;
                                }))
                        .subscribe(
                          data =>
                          {
                              newReq = request.clone({
                                  headers =
                                      this.auth.buildAuthenticationHeaders(
                                            "application/json", this.auth.currentUserValue.access_token).headers
                              });
                              return next.handle(newReq);
                          },
                          error => {

                            console.log(error);
                            errorHolder.errorMsg = error.error.error_description;
                          }

                        )*/
                }
            )
        )
    }
}
