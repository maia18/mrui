import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import {from, Observable} from 'rxjs';
import { AuthenticationService } from '../_services';
import {map} from "rxjs/operators";
import {PatientInfo, MedicalRecord} from "../_model/patientsmodel";


const PATIENTS_ENDPOINT = "http://localhost:8080/api/patients";

@Injectable({ providedIn: 'root' })
export class PatientService
{
    constructor(private http: HttpClient, private auth: AuthenticationService) {
    }

    addMedicalRecord(comment: string, patientId: string)
    {

      let url = 'http://localhost:8080/api/medrec/' + patientId;

      return this.http.put(
        url,
        {studyName: "General", diagnosisDescription: comment, diagnosisComments: ""},
        {headers: this.auth.buildAuthHeaders().headers, observe: "response"});
    }

    createPatient(json: string, authToken: string){
        const bearerTokenHeader = {
            headers: new HttpHeaders({
                'Content-Type':  'application/json',
                'Authorization': 'Bearer ' + authToken
            })
        };
        const options = {
            headers: bearerTokenHeader.headers,
            observe: "response" as 'body', // to display the full response & as 'body' for type cast
        };
        return this.http.post(`http://localhost:8080/api/patients`, json, {headers: bearerTokenHeader.headers, observe: "response"});
    }

    getPatientInfoWithRecords(patientId: string)
    {
      let url = 'http://localhost:8080/api/patients/' + patientId + '/details';
      return this.http.get<GeneralPatientInfo>(url, {headers: this.auth.buildAuthHeaders().headers, observe: "response"})
        .pipe(
          map(res => {
            return res.body as GeneralPatientInfo;
          }));
    }

    getPatientInfo(patientId: string, authToken:string){
        const bearerTokenHeader = {
            headers: new HttpHeaders({
                //'Content-Type':  'application/json',
                'Authorization': 'Bearer ' + authToken
            })
        };
        const options = {
            headers: bearerTokenHeader.headers,
            observe: "response" as 'body', 
        };
        let url = 'http://localhost:8080/api/patients/' + patientId;
        return this.http.get(url, {headers: bearerTokenHeader.headers, observe: "response"})
    }

    putPatientInfo(json: string, patientID: string, authToken: string)
    {
        const bearerTokenHeader = {
            headers: new HttpHeaders({
                'Content-Type':  'application/json',
                'Authorization': 'Bearer ' + authToken
            })
        };   
        const options = {
            headers: bearerTokenHeader.headers,
            observe: "response" as 'body', // to display the full response & as 'body' for type cast
        };   
        let url =  'http://localhost:8080/api/patients/' + patientID;
        return this.http.put(url, json, {headers: bearerTokenHeader.headers, observe: "response"});
    }

  retrieveAllPatients(document: string, name: string, lastname: string) : Observable<PatientInfo[]>
  {
    const requestUrl = `${PATIENTS_ENDPOINT}?document=${document}&name=${name}&lastname=${lastname}`;

    return this.http.get<PatientInfo[]>(
      requestUrl, {headers: this.auth.buildAuthHeaders().headers, observe: "response"})
      .pipe(
        map(res => {
          return res.body as PatientInfo[];
        }));
  }

}

export interface GeneralPatientInfo
{
  patient: PatientInfo,
  medicalRecords: MedicalRecord[]
}
