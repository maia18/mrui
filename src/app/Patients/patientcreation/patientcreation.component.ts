import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, FormControl, FormGroupDirective, NgForm, Validators, FormsModule} from '@angular/forms';
import { PatientService } from '../../_services/PatientService';
import {AuthenticationService} from '../../_services/AuthenticationService';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-patientcreation',
  templateUrl: './patientcreation.component.html',
  styleUrls: ['./patientcreation.component.css']
})
export class PatientcreationComponent implements OnInit {

	patCreationForm: FormGroup;  
	created: boolean=false;
	exists: boolean=false;
	error: boolean=false;
	minDate = new Date(1920, 0, 1);
	maxDate = new Date();

  constructor(private fb: FormBuilder, private patientPost: PatientService, private userAuth: AuthenticationService, private router: Router) {

	this.patCreationForm = fb.group({  
	  'docType': [null, Validators.required],
      'docId': [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])], 
      'name' : [null, Validators.required],  
	  'lastname': [null, Validators.required], 
	  'birthdate': [null, Validators.required],
	  'birthplace': [null, Validators.required],
	  'gender': [null, Validators.required],
	  'address': [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])],
	  'status': [null, Validators.required],
	  'phone': [null, Validators.compose([Validators.required, Validators.minLength(10)])],
	  'email': [null, Validators.compose([Validators.required, Validators.email])],
	  'child': [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(10)])]
    });  
	

  }

  ngOnInit() {
  }

  formatDate(date: Date): String
  {
    let d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('/');
  }

  onFormSubmit(form:any)
  {  
    console.log(form)

    if (this.patCreationForm.invalid)
    {
      return;
    }

    let date = form.birthdate;
    form.birthdate = this.formatDate(date);
    let errorHolder = {errorMsg : ''}
    let user = JSON.parse(localStorage.getItem('currentUser'));
    this.patientPost.createPatient(JSON.stringify(form), user.access_token).subscribe(
      resp => {
        if(resp.status == 201){
            console.log("Patient created sucessfuly");
            this.created = true;
            return;
        }else{
          this.error = true;
          return
        }
        console.log(resp)},
      err => {
        //If error, refresh token and attempt to create again
        if(err.status == 409){ //Patient already exists
          console.log("Patient already exists");
          this.exists=true;
        }else{
          console.log(err);
          //this.userAuth.refreshToken(errorHolder, () => this.createPatient(form));
          return;
        }
        }
    );
  }
}
