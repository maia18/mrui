import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material";
import {GeneralPatientInfo, PatientService} from "../../_services/PatientService";
import {MedicalRecord} from "../../_model/patientsmodel";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthenticationService} from "../../_services";


@Component({
  selector: 'app-searchuser',
  templateUrl: './patientsview.component.html',
  styleUrls: ['./patientsview.component.css']
})

export class PatientsviewComponent implements OnInit
{
    created: boolean=false;
    exists: boolean=false;
    error: boolean=false;
    user: GeneralPatientInfo;

    mainId: string = '';
    name: string='';
    id: string = '';
    docType: string = '';
    docId: string = '';
    gender: string = '';
    birthdate: string= '';
    birthplace: string = '';
    address : string='';
    phone: string='';
    email: string='';
    status: string='';
    child: string='';
    records: MedicalRecord[]=[];

    constructor(private fb: FormBuilder, private patientService: PatientService, public dialog: MatDialog,
                private route: ActivatedRoute, private userAuth: AuthenticationService)
    {
      this.route.paramMap.subscribe(params => {
        this.mainId = params.get("patientId")
        this.loadUser();
      })
    }

    loadUser()
    {
      let users = this.patientService.getPatientInfoWithRecords(this.mainId);
      users.subscribe(
        respo =>
        {
          this.user = respo;
          this.setPatientData();
        }
      )
    }
    setPatientData()
    {
      this.name = (this.user.patient.name + " " + this.user.patient.lastname);
      this.id = this.user.patient.id;
      this.docType = this.user.patient.docType;
      this.docId = this.user.patient.docId;
      this.gender = this.user.patient.gender === 'M'? "Male" : this.user.patient.gender === 'F'? "Female" : "Other";
      this.birthdate = this.user.patient.birthdate;
      this.birthplace = this.user.patient.birthplace;
      this.address = this.user.patient.address;
      this.phone = this.user.patient.phone;
      this.email = this.user.patient.email;
      this.status = this.user.patient.status;
      this.child = this.user.patient.child;
      this.records = this.user.medicalRecords;
    }
    ngOnInit()
    {
    }

    openDialog(): void {
        const dialogRef = this.dialog.open(AddEntryDialog, {
          width: '400px',
          height: '500px',
          data: {name: this.user.patient.name, lastname: this.user.patient.lastname, userId: this.mainId}
        });
        dialogRef.afterClosed().subscribe(
            result=>
            {
                this.loadUser();
            }
        )
    }
}

export interface PatientEntry
{
    id: number;
    fullname: string;
    documentype: string;
    document: string;
    edit: string;
    view: string;
}


export interface DialogData {
  lastname: string;
  name: string;
  comment: string;
  userId: string
}

@Component({
  selector: 'addEntry',
  templateUrl: 'addEntry.html',
})
export class AddEntryDialog {

  constructor(
    public dialogRef: MatDialogRef<AddEntryDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private patientService : PatientService) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSaveComment(): void
  {
    this.patientService.addMedicalRecord(this.data.comment, this.data.userId).subscribe(
      resp => {
        if(resp.status == 200){ //User created
          console.log("Record added");
        }
        this.dialogRef.close();
        console.log(resp)
        return;
      },
      err => {
        console.log(err);
        return;
      }
    );
  }
}
