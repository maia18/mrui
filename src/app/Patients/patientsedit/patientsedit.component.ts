import {Component, Inject, OnInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PatientService } from 'src/app/_services/PatientService';
import { AuthenticationService } from 'src/app/_services';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';


@Component({
  selector: 'app-patientsedit',
  templateUrl: './patientsedit.component.html',
  styleUrls: ['./patientsedit.component.css']
})
export class PatientseditComponent implements OnInit {

  patientEditionForm: FormGroup; 
  updated: boolean=false;
  error: boolean=false;
  notFound: boolean=false;
  minDate = new Date(1920, 0, 1);
  maxDate = new Date();

  patientId: string='';
  documentType: string='';
  doctTypeWithoutFormat: string='';
  documentNumber: string='';
  name: string='';
  lastname: string='';
  birthDate: Date= new Date();
  birthPlace: string='';
  gender: string='';
  address: string='';
  status: string='';
  phone: string='';
  email: string='';
  children: string='';

  constructor(private fb: FormBuilder, private userAuth: AuthenticationService, private route: ActivatedRoute, private thePatientService: PatientService, private router: Router) { 

	this.patientEditionForm = fb.group({  
	  'docType': [null, Validators.required],
    'docId': [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])], 
    'name' : [null, Validators.required],  
	  'lastname': [null, Validators.required], 
	  'birthdate': [null, Validators.required],
	  'birthplace': [null, Validators.required],
	  'gender': [null, Validators.required],
	  'address': [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])],
	  'status': [null, Validators.required],
	  'phone': [null, Validators.compose([Validators.required, Validators.minLength(10)])],
	  'email': [null, Validators.compose([Validators.required, Validators.email])],
	  'child': [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(10)])]
    }); 
  }

  ngOnInit() {
    let errorHolder = {errorMsg : ''};
    this.route.paramMap.subscribe(params => {
      this.patientId = params.get("patientId")
    })
    let user = JSON.parse(localStorage.getItem('currentUser'));
    this.thePatientService.getPatientInfo(this.patientId, user.access_token).subscribe(
      resp => {
        if(resp.status == 404)
        {
          console.log("Patient not found");
        }
        console.log(resp.body);
        //Set values of variables based on GET response
        this.setVariablesFromGET(resp.body);
        //Set values of form (necessary so that on submit the values are actually taken)
        this.setFormValues(); 
      },
      err => {
        if(err.status==404)
        {
          this.notFound=true;
          return;
        }
        //this.userAuth.refreshToken(errorHolder, () => this.getPatientInfo());
      }

    )
  }

  getPatientInfo()
  {
    let user = JSON.parse(localStorage.getItem('currentUser'));
    this.thePatientService.getPatientInfo(this.patientId, user.access_token).subscribe(
      resp => {
        if(resp.status == 404)
        {
          console.log("User not found");
          this.notFound = true;
        }
        console.log(resp.body);
        
        //Set values for variables according to http GET response
        this.setVariablesFromGET(resp.body);
        //Set values of form
        this.setFormValues();
      },
      err => {
        if(err.status==404)
        {
          this.notFound=true;
          return;
        }
        console.log(err);
      }
    )
  }

  setVariablesFromGET(response: Object)
  {
    this.documentType = this.formatDocType(response['docType']);
    this.doctTypeWithoutFormat = response['docType'];
    this.documentNumber = response['docId'];
    this.lastname = response['lastname'];
    this.name = response['name'];
    this.gender = response['gender'];
    this.birthDate = new Date(Date.parse(response['birthdate']));
    this.birthPlace = response['birthplace'];
    this.address = response['address'];
    this.phone = response['phone'];
    this.email = response['email'];
    this.status = response['status'];
    this.children = response['child'];
  }

  setFormValues()
  {
    this.patientEditionForm.controls['docType'].setValue(this.doctTypeWithoutFormat);
    this.patientEditionForm.controls['docId'].setValue(this.documentNumber);
    this.patientEditionForm.controls['name'].setValue(this.name);
    this.patientEditionForm.controls['lastname'].setValue(this.lastname);
    this.patientEditionForm.controls['birthdate'].setValue(this.birthDate);
    this.patientEditionForm.controls['birthplace'].setValue(this.birthPlace);
    this.patientEditionForm.controls['gender'].setValue(this.gender);
    this.patientEditionForm.controls['phone'].setValue(this.phone);
    this.patientEditionForm.controls['address'].setValue(this.address);
    this.patientEditionForm.controls['status'].setValue(this.status);
    this.patientEditionForm.controls['email'].setValue(this.email);
    this.patientEditionForm.controls['child'].setValue(this.children);
  }

  formatDocType(doctype: string)
  {
    switch(doctype) {
      case "PASSPORT":
          return "Passport";
      case "NATIONAL_ID":
          return "National ID";
      case "RESIDENCE_PERMIT_NUMBER":
          return "Residence Permit";
      default:
          return "Other";
    }
  }

  formatDate(date: Date): String
  {
    let d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('/');
  }

  onFormSubmit(form: any)
  {
      console.log(form);

      if (this.patientEditionForm.invalid) 
      {
        return;
      }

      let date = form.birthdate;
      form.birthdate = this.formatDate(date);

      //First, try to create with current token
      let errorHolder = {errorMsg : ''}
      let user = JSON.parse(localStorage.getItem('currentUser'));
      this.thePatientService.putPatientInfo(JSON.stringify(form), this.patientId, user.access_token).subscribe(
        resp => {
          if(resp.status == 200){ //User created
              console.log("User updated sucessfuly");
              this.updated = true;
              return;
          }else{
            this.error = true;
            return
          }
          console.log(resp)},
        err => {
          //If error, refresh token and attempt to create again
          if(err.status == 409){ //User already exists
            console.log("this");
          }else{
            console.log(err);
            console.log("Trying to refresh token");
            //this.userAuth.refreshToken(errorHolder, () => this.updateUser(form));
            return;
          }
          }
      );

  }

  updateUser(form: NgForm)
  {
    let user = JSON.parse(localStorage.getItem('currentUser'));
      this.thePatientService.putPatientInfo(JSON.stringify(form), this.patientId, user.access_token).subscribe(
        resp => {
          if(resp.status == 200){ //User created 
              console.log("User updated sucessfuly");
              this.updated = true;
              return;
          }else{
            this.error = true;
            return
          }
          console.log(resp)},
        err => {
            console.log(err);
            return;
          } 
      );
  }

  goToPatient()
  {
    this.router.navigate(['/patients/view/'+this.patientId]);
  }
}
