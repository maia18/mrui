import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, FormControl, FormGroupDirective, NgForm, Validators, FormsModule} from '@angular/forms';
import {UserInfo, UserService} from '../../_services/UserService';
import {AuthenticationService} from '../../_services/AuthenticationService';
import { Router, ActivatedRoute } from '@angular/router';
import {MatTableDataSource} from "@angular/material";
import {of} from "rxjs";
import {mapEntry} from "@angular/compiler/src/output/map_util";
import {PatientService} from "../../_services/PatientService";


@Component({
  selector: 'app-searchuser',
  templateUrl: './patientssearch.component.html',
  styleUrls: ['./patientssearch.component.css']
})

export class PatientssearchComponent implements OnInit
{
    userSearchForm: FormGroup;
    created: boolean=false;
    exists: boolean=false;
    error: boolean=false;
    displayedColumns = ['userid', 'fullname', 'documentype', 'document', 'action'];
    dataSource = ELEMENT_DATA;


    constructor(private fb: FormBuilder, private patientService: PatientService, private userAuth: AuthenticationService)
    {
      this.userSearchForm =
        fb.group({
          'userId': [""],
          'documentNumber': [""],
          'name' : [""],
          'lastName': [""]
          });

    }
    ngOnInit()
    {
        let users = this.patientService.retrieveAllPatients("","","");
        users.subscribe(
            respo =>
                {
                    let entries = this.mapEntries(respo);
                    this.dataSource = entries;
                }
        )
    }

    private mapEntries(patients: any) : PatientEntry[]
    {
        let entries: PatientEntry[] = new Array(patients.length);
        for (var i = 0; i < patients.length; i++)
        {
            let entry : PatientEntry = new class implements PatientEntry {
                document: string;
                documentype: string;
                fullname: string;
                id: number;
                edit: string;
                view: string;
            };
            entry.document = patients[i].docId;
            entry.documentype = patients[i].docType;
            entry.fullname = patients[i].name + " " + patients[i].lastname;
            entry.id = +(patients[i].id);
            entry.view = "/patients/view/" + patients[i].id;
            entry.edit = "/patients/edit/" + patients[i].id;
            entries[i] = entry;
        }
        return entries
    }

    onFormSubmit(form: UserInfo)
    {
        let users =
            this.patientService.retrieveAllPatients(form.documentNumber, form.name,form.lastName);
        users.subscribe(
            respo =>
            {
                let entries = this.mapEntries(respo);
                this.dataSource = entries;
            }
        )
    }
}

export interface PatientEntry
{
    id: number;
    fullname: string;
    documentype: string;
    document: string;
    edit: string;
    view: string;
}

const ELEMENT_DATA: PatientEntry[] = [
  {id: 1, fullname: 'Laura Pausini', documentype: 'Passport', document: 'AOPLL', edit: '/hello', view: '/hello'}
];
