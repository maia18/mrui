import {NgModule} from "@angular/core";
import { CommonModule } from '@angular/common';
import {
  MatButtonModule, MatCardModule, MatDialogModule, MatInputModule, MatTableModule, MatNativeDateModule,
  MatToolbarModule, MatMenuModule, MatIconModule, MatProgressSpinnerModule, MatSidenavModule, MatGridListModule, MatFormFieldModule, MatSelectModule, MatDatepickerModule
} from '@angular/material';
@NgModule({
  imports: [
  CommonModule, 
  MatToolbarModule,
  MatButtonModule, 
  MatCardModule,
  MatInputModule,
  MatDialogModule,
  MatTableModule,
  MatMenuModule,
  MatSidenavModule,
  MatIconModule,
  MatGridListModule,
  MatProgressSpinnerModule,
  MatFormFieldModule, 
  MatSelectModule, 
  MatDatepickerModule,
  MatNativeDateModule
  ],
  exports: [
  CommonModule,
   MatToolbarModule, 
   MatButtonModule, 
   MatCardModule, 
   MatInputModule, 
   MatDialogModule, 
   MatTableModule, 
   MatMenuModule,
   MatSidenavModule,
   MatIconModule,
   MatGridListModule,
   MatProgressSpinnerModule,
   MatFormFieldModule, 
   MatSelectModule,
   MatDatepickerModule,
   MatNativeDateModule
   ],
})
export class CustomMaterialModule { }