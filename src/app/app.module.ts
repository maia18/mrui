import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CustomMaterialModule } from './_core/material.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login';
import { TestresComponent} from "./testres";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { PatientcreationComponent } from './Patients/patientcreation/patientcreation.component';
import {ErrorStateMatcher,ShowOnDirtyErrorStateMatcher} from '@angular/material';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { TextareaAutosizeModule } from 'ngx-textarea-autosize';
import { UsercreationComponent } from './users/create/usercreation.component';
import { UsersviewComponent } from './users/usersview/usersview.component';
import { UserseditComponent } from './users/usersedit/usersedit.component';
import {UsersearchComponent} from "./users/search/usersearch.component";
import { PatientseditComponent } from './Patients/patientsedit/patientsedit.component';
import { ProfileComponent } from './users/profile/profile.component';
import { ChangepasswordComponent } from './users/changepassword/changepassword.component';
import {PatientssearchComponent} from "./Patients/search/patientssearch.component";
import {PatientsviewComponent, AddEntryDialog} from "./Patients/view/patientsview.component";


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TestresComponent,
    PatientcreationComponent,
    UsercreationComponent,
    UsersviewComponent,
    UserseditComponent,
    PatientseditComponent,
    ProfileComponent,
    ChangepasswordComponent,
    UsersearchComponent,
    PatientseditComponent,
    PatientssearchComponent,
    PatientsviewComponent,
    AddEntryDialog
  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    CustomMaterialModule,
    TextareaAutosizeModule
  ],
  entryComponents: [PatientsviewComponent, AddEntryDialog],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
  bootstrap: [AppComponent]
})
export class AppModule { }
